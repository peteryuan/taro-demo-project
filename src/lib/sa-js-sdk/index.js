import sa from 'sa-sdk-javascript';

export function initSa() {
  console.log('初始化神策js sdk');
  sa.init({
    server_url: `https://sensor.boldseas.com/sa?project=${SA_ENV}`,
    heatmap: {
      //是否开启点击图，默认 default 表示开启，自动采集 $WebClick 事件，可以设置 'not_collect' 表示关闭
      clickmap: 'default',
      //是否开启触达注意力图，默认 default 表示开启，自动采集 $WebStay 事件，可以设置 'not_collect' 表示关闭
      scroll_notice_map: 'not_collect'
    }
  });
}

export function loginSa(phone) {
  console.log('登录神策js sdk', phone)
  sa.login(phone);
}

export function autoTrack() {
  console.log('神策js sdk快速追踪')
  sa.quick('autoTrack');
}

export function setProfileSa(profileInfo) {
  sa.setProfile(profileInfo);
}

export function getQuerySa() {
  return sa.getPresetProperties();
}

export default {
  initSa,
  loginSa,
  autoTrack,
  getQuerySa
}