import sa from './sensorsdata';
import cf from '../../common-func';
import Taro from '@tarojs/taro'
/**
 * @description 神策通过手机号登录初始化
 * @author peter.yuan
 * @export
 * @param {*} mobilePhone
 */
export function loginWithMobilePhone(mobilePhone) {
  const distinctId = sa.store.getDistinctId();
  sa.login(mobilePhone);
  console.log(distinctId, mobilePhone, '神策初始化');
  sa.init();
  sa.identify(mobilePhone);
}
/**
 * @description 使用code换回来的openId 匿名初始化神策
 * @author peter.yuan
 * @export
 * @param {*} openid
 */
export function anonymousInitWithOpenId(openid) {
  console.log(openid, '神策初始化');
  sa.setOpenid(openid);
  sa.init();
  sa.identify(openid);
}
/**
 * @description 默认添加了小程序APPId和Name的track 方法
 * @author peter.yuan
 * @export
 * @param {*} eventName
 * @param {*} params
 * @returns
 */
export function defaultTrack(eventName, params) {
  return sa.track(eventName, params);
}
/**
 * @description 获取神策事件的基础属性
 * @author peter.yuan
 * @export
 * @returns
 */
export function getBasicPropertiesForEvent() {
  const user = cf.getCurrentUserFromStorage();
  return {
    presetProperties: sa.getPresetProperties(),
    tenant_id: TENANT_ID,
    user_id: user ? user.id : '',
    app_id: APP_ID,
    app_name: '我的Jeep攻略'
  };
}
/**
 * @description 获取当前页面的url
 * @author peter.yuan
 * @export
 * @returns
 */
export function getCurrentPagePath() {
  const len = Taro.getCurrentPages().length;
  return Taro.getCurrentPages()[len - 1].route;
}
/**
 * @description 获取当前页面的query参数
 * @author peter.yuan
 * @export
 * @returns
 */
export function getCurrentPageQuery() {
  const len = Taro.getCurrentPages().length;
  const options = Taro.getCurrentPages()[len - 1].options;
  return options;
}

export function genTrackUserAuthEventParams() {
  return {
    scope: '',//比如"scope.userInfo"，对于手机号授权比较特殊，叫做"scope.phone"，其他见https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/authorize.html#scope-%E5%88%97%E8%A1%A8
    accept: '',//true/false
    reason: '',//为了什么功能需要授权，比如"login"、"appointment-sign",
    from: '',//为了知道用户是在什么位置进行的授权，添加来源字段
  }
}

export function trackAuthorizeEvent(params) {
  const _params = {
    ...params,
    ...getBasicPropertiesForEvent(),

  }
  console.log('==========>>>神策追踪授权<<<==========', _params);
  sa.track('Ma_User_Authorize_Event', _params);
}

export function genTrackUserCommitFormEventParams() {
  return {
    form_name: '',// 表单名称（预约留资表单）
    form_element_id: '',//表单元素的id
    form_element_class: '',//表单元素的class
    form_submit_url: '',//提交form的地址
    form_submit_id: '',//表单提交后返回的id（如果有的话）
    form_submit_success: '',//true/false
    form_submit_gender: '',//用户的性别
    form_submit_interest_car: '',// 用户感兴趣的车型
    form_submit_dealer: '',// 客户感兴趣的经销商
    form_submit_dealer_city: '',//客户感兴趣经销商所在城市，例如”朝阳区“
    form_submit_dealer_province: '',// 客户感兴趣经销商锁在省，例如”北京“
    form_submit_appointment_date: '',//客户预约的时间
  };
}

export function trackCommitFormEvent(params) {
  const _params = {
    ...params,
    ...getBasicPropertiesForEvent(),

  }
  console.log('==========>>>神策追踪表单提交<<<==========', _params);
  sa.track('Ma_Form_Submit', _params);
}

export function genTrackUserClickEventParams(params) {
  return {
    element_content: '',//比如button中的文本内容，比如”确定“
    element_id: '',//标志在当前页面唯一的id，可以是代码中的id属性
    element_type: '',//元素类型，比如"button"
    element_selector: '',//元素选择器
    element_target_url: '',//元素链接地址
    element_class_name: '',//元素类名
    element_dataset: ""
  };
}

export function trackUserClickEvent(params) {
  const _params = {
    ...params,
    ...getBasicPropertiesForEvent(),

  }
  console.log('==========>>>神策追踪用户点击事件<<<==========', _params);
  sa.track('Element_Click_Event', _params);
}

export function getShareQueryPath() {
  let latest_utm_source = sa.getPresetProperties().$latest_utm_source;
  return latest_utm_source ? `?utm_campaign=ma&utm_source=share&utm_medium=chat&utm_content=${latest_utm_source}` : '?utm_campaign=ma&utm_source=share&utm_medium=chat';
}

export default {
  loginWithMobilePhone,
  anonymousInitWithOpenId,
  trackAuthorizeEvent,
  trackCommitFormEvent,
  trackUserClickEvent,
  getBasicPropertiesForEvent,
  getCurrentPageQuery,
  getCurrentPagePath
}