import http from '../http';
import cf from '../common-func';

export async function submitFormId(formId) {
  const openId = cf.getOpenIdFormStorage();
  if (openId) {
    const data = { openId, formId };
    await http.post(`${HOST}/api/wechat/mini-program/${APP_ID}/formid`, { data })
  } else {
    console.log('不存在openId,无法提交formId');
  }
}

export default {
  submitFormId
}