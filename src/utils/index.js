import maLoginUtilModule from './ma-login';
import maUserUtilModule from './ma-user';
import maObjectUtilModule from './ma-object';
import maWechatUtilModule from './ma-wechat';
export const maLoginUtil = maLoginUtilModule;
export const maUserUtil = maUserUtilModule;
export const maObjectUtil = maObjectUtilModule;
export const maWechatUtil = maWechatUtilModule;







export default {
  maLoginUtil,
  maUserUtil,
  maObjectUtil,
  maWechatUtil
}