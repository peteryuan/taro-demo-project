
import http from '../http';
/**
 * @description 根据code 和租户id登录
 * @author peter.yuan
 * @export
 * @param {*} code
 * @param {*} tenantId
 * @returns
 */
export function loginWithWechatCode(code) {
  return http.post(`${HOST}/api/iam/${APP_ID}/code/_login`, { data: { code, tenantId: TENANT_ID } })
}
/**
 * @description 退出登录
 * @author peter.yuan
 * @export
 * @param {*} params
 * @returns
 */
export function logout(params) {
  return http.get(`${HOST}/api/iam/_logout`);
}
/**
 * @description 授权手机号登录
 * @author peter.yuan
 * @export
 * @param {*} data: {
                      "encryptedData": "CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZMQmRz==",
                      "iv": "r7BXXKkLb8qrSNn05n0qiA==",
                      "tenantId": "boldseas",
                      "userType": "CustomerPortalUser"
                    }
 * @returns
 */
export function loginWithEncryptedPhoneData(data) {
  return http.post(`${HOST}/api/iam/phone/_login`, { data });
}
/**
 * @description 根据加密手机号信息生成手机号登陆的Data
 * @author peter.yuan
 * @export
 * @param {*} encryptedData
 * @param {*} iv
 * @returns
 */
export function genEncryptedPhoneData(encryptedData, iv) {
  return {
    encryptedData,
    iv,
    tenantId: TENANT_ID,
    userType: "CustomerPortalUser"
  };
}
/**
 * @description 获取验证码
 * @author peter.yuan
 * @export
 * @param {*} data: {
                      "countryCode": "string",
                      "phone": "string",
                      "tenantId": "string"
                     }
 */
export function getPinByPhoneData(data) {
  return http.post(`${HOST}/api/iam/pin`, { data });
}
/**
 * @description 根据电话号码和国家码生成获取验证码Data
 * @author peter.yuan
 * @export
 * @param {*} phone
 * @param {string} [countryCode='86']
 * @returns
 */
export function genPhoneData(phone, countryCode = '86') {
  return {
    phone,
    countryCode,
    tenantId: TENANT_ID
  }
}

/**
 * @description 通过验证码登录 此方法后端会默默的创建User 如果该手机号不存在的话在库内
 * @author peter.yuan
 * @export
 * @param {*} data ：{
                      "countryCode": "string",
                      "phone": "string",
                      "pin": "string",
                      "tenantId": "string",
                      "userType": "CustomerPortalUser"
                    }
 * @returns
 */
export function loginWithPinData(data) {
  return http.post(`${HOST}/api/iam/pin/_login`, { data })
}
/**
 * @description 根据电话号码 验证码和国家码生成 验证码登录的Data
 * @author peter.yuan
 * @export
 * @param {*} phone
 * @param {*} pin
 * @param {string} [countryCode='86']
 * @returns
 */
export function genPinData(phone, pin, countryCode = '86') {
  return {
    countryCode,
    phone,
    pin,
    tenantId: TENANT_ID,
    "userType": "CustomerPortalUser"
  };
}
/**
 * @description 检查后盾数据库是否存在session 此方法和 loginWithWechatCode 紧密相连
 * @author peter.yuan
 * @export
 * @returns
 */
export function checkSession() {
  return http.get(`${HOST}/api/session`);
}

export default {
  loginWithWechatCode,
  logout,
  loginWithEncryptedPhoneData,
  getPinByPhoneData,
  loginWithPinData,
  genEncryptedPhoneData,
  genPhoneData,
  genPinData,
  checkSession
}