import http from '../http';
import cf from '../common-func';
/**
 * @description 在登陆后获取当前登录用户的信息
 * @author peter.yuan
 * @export
 * @returns
 */
export async function getCurrentUser() {
  const user = cf.getCurrentUserFromStorage();
  if (user) {
    return user;
  } else {
    try {
      const res = await http.get(`${HOST}/api/user`);
      cf.setCurrentUserToStorage(res);
      return res;
    } catch (error) {
      cf.showNoCancelModal('获取当前用户出错');
      return null;
    }
  }
}




export default {
  getCurrentUser
}