import http from '../http';
/**
 * @description 根据ObjectName创建对应的实体
 * @author peter.yuan
 * @export
 * @param {*} objectName
 * @param {*} data
 * @returns
 */
export function createObject(objectName, data) {
  return http.post(`${HOST}/api/v1/objects/${objectName}`, { data });
}
/**
 * @description 根据ObjectName 查询数据
 * @author peter.yuan
 * @export
 * @param {*} objectName
 * @param {*} data
 * @param {*} queryOption
 * @returns
 */
export function queryObject(objectName, data, queryOption) {
  let queryStr = '';
  if (queryOption) {
    const { limit, skip } = queryOption;
    queryStr = `?limit=${limit}&skip=${skip}`;
  }
  return http.post(`${HOST}/api/v1/objects/${objectName}/_query${queryStr}`, { data });
}
/**
 * @description 获取某个对象结果
 * @author peter.yuan
 * @export
 * @param {*} objectName
 * @param {*} id
 * @param {*} queryOptionArray ：['_id','name',...]
 * @returns
 */
export function getObjectById(objectName, id, queryOptionArray) {
  let queryStr = '';
  if (queryOptionArray) {
    queryStr = '?';
    queryOptionArray.forEach(k => {
      queryStr += `properties=${k}`;
    });
  }
  return http.get(`${HOST}/api/v1/objects/${objectName}/${id}${queryStr}`);
}

export default {
  createObject,
  queryObject,
  getObjectById
}