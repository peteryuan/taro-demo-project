import { observable } from 'mobx'
import Taro from '@tarojs/taro'


const homeStore = observable({
  counter: 0,
  gotoHome() {
    Taro.navigateTo({ url: '/pages/home/index' })
  },
  gotoActivityInfo() {
    Taro.navigateTo({ url: '/pages/activity/index' })
  }
})

export default homeStore