import { warningTexts } from "../constants";

const SESSION = 'SESSION';
const CURRENT_USER = 'CURRENT_USER';
const OPEN_ID = 'OPEN_ID';
/**
 * @description 同步session到本地
 * @author peter.yuan
 * @export
 * @param {*} session
 */
export function setSessionToStorage(session) {
  wx.setStorageSync(SESSION, session);
}

/**
 * @description 从本地获取存储的session
 * @author peter.yuan
 * @export
 * @returns
 */
export function getSessionFromStorage() {
  return wx.getStorageSync(SESSION);
}
/**
 * @description 将用户信息存储到本地
 * @author peter.yuan
 * @export
 * @param {*} user
 */
export function setCurrentUserToStorage(user) {
  wx.setStorageSync(CURRENT_USER, JSON.stringify(user));
}
/**
 * @description 从本地存储获取当前登录用户信息
 * @author peter.yuan
 * @export
 * @returns
 */
export function getCurrentUserFromStorage() {
  const userStr = wx.getStorageSync(CURRENT_USER);
  return userStr ? JSON.parse(userStr) : null;
}

export function setOpenIdToStorage(openId) {
  wx.setStorageSync(OPEN_ID, openId);
}

export function getOpenIdFormStorage() {
  return wx.getStorageSync(OPEN_ID);
}

export function validPhone(phone) {
  if (phone && phone.length === 11) {
    return /^1[3|4|5|7|8|9][0-9]\d{4,8}$/.test(phone);
  } else {
    showNoCancelModal(warningTexts.PHONE_UNVALID_TEXT);
    return false;
  }
}

export function cloneObject(obj) {
  return JSON.parse(JSON.stringify(obj));
}

export function showNoCancelModal(content = '', title = '提示') {
  wx.showModal({
    title, //提示的标题,
    content, //提示的内容,
    showCancel: false, //是否显示取消按钮,
    cancelText: '取消', //取消按钮的文字，默认为取消，最多 4 个字符,
    cancelColor: '#000000', //取消按钮的文字颜色,
    confirmText: '确定', //确定按钮的文字，默认为取消，最多 4 个字符,
    confirmColor: '#3CC51F', //确定按钮的文字颜色,
  });
}

export default {
  setSessionToStorage,
  getSessionFromStorage,
  setCurrentUserToStorage,
  getCurrentUserFromStorage,
  setOpenIdToStorage,
  getOpenIdFormStorage,
  validPhone,
  cloneObject,
  showNoCancelModal
}