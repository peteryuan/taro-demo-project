import warningTextModule from './warning-text';

export const warningTexts = warningTextModule;
export const LOGO_URL = 'https://www.bentleymotors.com/etc/clientlibs/bentley/bentley.motors.apps/resources/img/logo@2x.png';
export const CAR_PICTURE_URL = 'https://www.bentleymotors.com/content/dam/bentley/Master/Models/Hero/Flying%20Spur%20V8%20S/V8S_Franschoek_Pass%20Flying%20Spur%20V8%20S.jpg/_jcr_content/renditions/original./V8S_Franschoek_Pass%20Flying%20Spur%20V8%20S.jpg';
export default {
  warningTexts,
  LOGO_URL,
  CAR_PICTURE_URL
}