export const PHONE_UNVALID_TEXT = '手机号格式有误';
export const PIN_UNVALID_TEXT = '验证码输入有误';

export default {
  PHONE_UNVALID_TEXT,
  PIN_UNVALID_TEXT
}