import Taro, { Component } from '@tarojs/taro'
import { View, Picker, Progress, Text, Button, Input, Image } from '@tarojs/components'
import './index.scss'
import TButton from '@components/TButton';
import { maLoginUtil, maUserUtil, maObjectUtil } from '../../utils';
import WeChatUserInfo from '../../components/WeChatUserInfo';
// import sa from '../../lib/sa';
import cf from '../../common-func';
import { warningTexts } from '../../constants';
import { observer, inject } from '@tarojs/mobx'
import FlexRowLabelWithIcon from '../../components/FlexRowLabelWithIcon';
import { autoTrack } from '../../lib/sa-js-sdk';
import ButtonWithArrow from '../../components/ButtonWithArrow';
import CommonBanner from '../../components/CommonBanner';
@inject('homeStore')
@observer
export default class Index extends Component {

  state = {
    rangeArr: ['a', 'b', 'c'],
    pickValue: 'a',
    phone: '',
    pin: '',
    userInfo: null
  }

  config = {
    navigationBarTitleText: '宾利汽车中国'
  }

  componentWillMount() { }

  componentDidMount() {
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  onPickerChange = e => {
    this.setState({
      pickValue: this.state.rangeArr[e.detail.value]
    });
  }

  getPhoneNumber = async e => {
    const errMsg = e.detail.errMsg;
    const encryptedData = e.detail.encryptedData;
    const iv = e.detail.iv;
    if (encryptedData && iv) {
      const data = maLoginUtil.genEncryptedPhoneData(encryptedData, iv)
      const res = await maLoginUtil.loginWithEncryptedPhoneData(data);
      const user = await maUserUtil.getCurrentUser();
      if (user) {
        // sa.loginWithMobilePhone(user.phone);
      }
      if (res) {
        Taro.navigateTo({ url: '/pages/demo/index' })
      } else {
        cf.showNoCancelModal('授权手机号失败');
      }
    } else {
      cf.showNoCancelModal(errMsg);
    }
  }

  wechatLogin = () => {
    wx.login({
      success: function (res) {
        // success
        const { code, errMsg } = res;
        if (code) {
          //wechat login ok
          maLoginUtil.loginWithWechatCode(code)
            .then((result) => {
              const { statusCode, errorMessage, wechat } = result;
              if (statusCode === 0) {
                const openId = wechat.openid;
                cf.setOpenIdToStorage(openId);
                // sa.anonymousInitWithOpenId(openId);
                cf.showNoCancelModal('微信Code登陆成功', '恭喜');
              } else {
                cf.showNoCancelModal(errorMessage, '抱歉');
              }
            }).catch((err) => {
              console.log(err)
            });
        }
      },
      fail: function () {
        // fail
        cf.showNoCancelModal('登录失败');
      }
    })
  }

  handlePhoneInput = e => {
    const phone = e.detail.value;
    this.setState({ phone })
  }

  handlePinInput = e => {
    const pin = e.detail.value;
    this.setState({ pin })
  }

  getPin = () => {
    const { phone } = this.state;
    if (cf.validPhone(phone)) {
      const phoneData = maLoginUtil.genPhoneData(phone, 86);
      maLoginUtil.getPinByPhoneData(phoneData)
        .then((result) => {
          console.log(result);
          wx.showToast({
            title: '验证码已发送', //提示的内容,
            icon: 'success', //图标,
            duration: 2000, //延迟时间,
            mask: true, //显示透明蒙层，防止触摸穿透,
          });
        }).catch((err) => {
          console.log(err);
        });
    }
  }

  loginWithPin = () => {
    const { phone, pin } = this.state;
    if (cf.validPhone(phone)) {
      if (pin) {
        const pinData = maLoginUtil.genPinData(phone, pin);
        maLoginUtil.loginWithPinData(pinData)
          .then((result) => {
            const { statusCode, errorMessage } = result;
            if (statusCode === 0 && !errorMessage) {
              cf.showNoCancelModal('登陆成功', '恭喜');
            } else {
              cf.showNoCancelModal(errorMessage);
            }
          }).catch((err) => {
            console.log(err);
          });
      } else {
        cf.showNoCancelModal(warningTexts.PIN_UNVALID_TEXT);
      }
    }
  }

  checkSession = async () => {
    try {
      await maLoginUtil.checkSession();
    } catch (error) {
      console.log('error', error)
    }
  }

  getCurrentUserAndInitSa = async () => {
    const user = await maUserUtil.getCurrentUser();
    if (user) {
      if (user.phone) {
        // sa.loginWithMobilePhone(user.phone)
      } else {
        const wechat = user.weChats.find(v => v.appId === APP_ID);
        cf.setOpenIdToStorage(wechat.openId);
        // sa.anonymousInitWithOpenId(wechat.openId);
      }
    }
  }


  getData = async () => {
    try {
      const res = await maObjectUtil.queryObject('Appointment', { project: { _id: 1 }, sort: {}, filter: { _id: '48d6f4b2e3c842fca3b74fecfd8ee293' } });
      res && cf.showNoCancelModal('获取成功', '恭喜')
    } catch (error) {
      console.log('error:', error);
    }
  }

  getDataById = async () => {
    try {
      const res = await maObjectUtil.getObjectById('User', '200a527daff14ddbad5789e47ee9ee50');
      res && cf.showNoCancelModal('获取成功', '恭喜')
    } catch (error) {
      console.log('error:', error);
    }
  }

  getWechatUserInfo = e => {
    const userInfo = e.detail.userInfo;
    if (userInfo) {
      cf.showNoCancelModal('获取成功', '恭喜');
      this.setState({ userInfo });
    }
  }

  saTrackAllEvent = e => {
    // sa.trackUserClickEvent(e);
    autoTrack();
  }

  gotoHome = () => {
    const { homeStore } = this.props
    homeStore.gotoHome()
  }

  gotoActivityInfo = () => {
    const { homeStore } = this.props
    homeStore.gotoActivityInfo()
  }

  render() {
    return (
      <View className='index'>
        <CommonBanner />
        <View className="home-title">
          <Text className="iconfont icon-baomingxinxi baomingxinxi"></Text> <Text className="home-title-label">我的报名信息</Text>
        </View>
        <View className="baoming-info">
          <FlexRowLabelWithIcon text="活动名称" value="宾利全新欧陆 GT 赛道体验日" iconName="iconfont icon-huodongmingcheng" />
          <FlexRowLabelWithIcon text="姓名" value="张三" iconName="iconfont icon-xingming" />
          <FlexRowLabelWithIcon text="称谓" value="先生" iconName="iconfont icon-chengwei" />
          <FlexRowLabelWithIcon text="手机号码" value="+86 18518981046" iconName="iconfont icon-shouji" />
          <FlexRowLabelWithIcon text="经销商" value="宾利杭州" iconName="iconfont icon-jingxiaoshang" />
          <FlexRowLabelWithIcon text="报名场次" value="绍兴站 2019年3月15日 上午场" iconName="iconfont icon-baomingchangci" />
        </View>
        <View className="btns">
          <ButtonWithArrow onClick={this.gotoHome} btnText="返回首页" />
          <ButtonWithArrow onClick={this.gotoActivityInfo} btnText="查看活动信息" />
        </View>
        <View className="warning">
          <Text className="warning-text">*如您对以上活动报名信息有任何疑问，请联系当地宾利授权经销商</Text>
        </View>
      </View>
    )
  }
}

