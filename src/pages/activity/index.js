import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import ButtonWithArrow from '../../components/ButtonWithArrow';
import CommonCarPic from '../../components/CommonCarPic';

export default class Activity extends Component {
  config = {
    navigationBarTitleText: '宾利汽车中国'
  }

  render() {
    return <View>
      <CommonCarPic />
      <View className="act-text-box">
        <Text className="act-text">
          此次活动为专属客户定向邀约，感谢您对宾利活动的关注与支持，更多精彩活动即将为您开启。
        </Text>
      </View>
      <View className="act-text-box">
        <Text className="act-text">
          如欲了解更多活动详情，您亦可莅临当地宾利授权经销商。
        </Text>
      </View>
      <View className="act-btn-box">
        <ButtonWithArrow customerClassName="btn-search" btnText="查询授权经销商" href="https://bentley-wechat.boldseas.com/static/dealerMap?null" />
      </View>
    </View>
  }
}
