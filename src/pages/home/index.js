import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components';
import { getQuerySa } from '../../lib/sa-js-sdk';
import NavItem from '../../components/NavItem';
import CommonCarPic from '../../components/CommonCarPic';
export default class extends Component {
  config = {
    navigationBarTitleText: '宾利汽车中国'
  }

  componentDidMount() {
  }

  goto = (path) => {
    console.log(path)
    Taro.navigateTo({ url: path });
  }

  render() {
    return <View>
      <CommonCarPic />
      <NavItem text="我的报名信息" onClick={() => this.goto('pages/index/index')} />
      <NavItem text="最新活动咨询" onClick={() => this.goto('pages/activity-info/index')} />
      <NavItem text="活动精彩图集" />
    </View>
  }
}