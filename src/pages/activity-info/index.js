import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import ButtonWithArrow from '../../components/ButtonWithArrow';
import CommonCarPic from '../../components/CommonCarPic';
import CommonBanner from '../../components/CommonBanner';

export default class Activity extends Component {
  config = {
    navigationBarTitleText: '宾利汽车中国'
  }
  render() {
    return (
      <View>
        <CommonBanner />
        <View className="banner-pic"></View>
        <View className="label-text">宾利全新欧陆 GT 赛道体验日行程安排</View>
        <View className="divide-line"></View>
        <View className="info-pic"></View>
      </View>
    );
  }
}