import { Button, Form } from "@tarojs/components";
import Taro, { Component } from '@tarojs/taro'
import { maWechatUtil } from "../utils";

export default class extends Component {
  constructor(props) {
    super(props);
  }

  handleClick = e => {
    //do some common action then : formId to backend
    maWechatUtil.submitFormId(e.detail.formId);
    this.props.onClick && this.props.onClick();
  }

  render() {
    const { className, style, text, disabled, openType } = this.props;
    return (
      <Form report-submit onSubmit={this.handleClick}>
        <Button form-type="submit" className={className} style={style} disabled={disabled} openType={openType}>
          {text}
        </Button>
      </Form>
    )
  }
}
