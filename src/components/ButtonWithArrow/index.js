import Taro, { Component } from '@tarojs/taro'
import './index.scss'
import { Text, View } from '@tarojs/components'

export default class ButtonWithArrow extends Component {

  handleClick = () => {
    console.log('click');
    this.props.onClick && this.props.onClick();
  }

  render() {
    const { btnText, href, customerClassName } = this.props;
    console.log(href)
    return <View className={`btn-container ${customerClassName}`}>
      {href ? <a className="btn" href={href}>{btnText}</a> : <Text className="btn" onClick={this.handleClick}>{btnText}</Text>}
      {href ? <a className="btn-icon" href={href}>></a> : <Text className="btn-icon iconfont icon-baijiantou" onClick={this.handleClick}></Text>}
    </View>
  }
}