import Taro, { Component } from '@tarojs/taro'
import './index.scss'
import { View } from '@tarojs/components';
export default class extends Component {

  render() {
    return <View className="flex-row-container">
      {this.props.children}
    </View>
  }
}