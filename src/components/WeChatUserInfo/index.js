import Taro, { Component } from '@tarojs/taro'
import { View, Image, Label, OpenData } from '@tarojs/components';

export default class WeChatUserInfo extends Component {

  render() {
    return (
      <View style={{ textAlign: 'center' }}>
        <View style={{ width: '120rpx', height: '120rpx', borderRadius: '50%', display: 'inline-block' }} >
          <OpenData type="userAvatarUrl" />
        </View>
        <Label style={{display:'block'}}>
          <OpenData type="userNickName" />
        </Label>
      </View>
    );
  }
}
