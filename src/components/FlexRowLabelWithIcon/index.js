import Taro, { Component } from '@tarojs/taro'
import './index.scss'
import { View, Text, Label } from '@tarojs/components'


export default class FlexRowLabelWithIcon extends Component {

  render() {
    const text = this.props.text;
    const tLen = text.length;
    const iconName = this.props.iconName;
    let tArr = [];
    for (let index = 0; index < tLen; index++) {
      const t = text[index];
      tArr.push(t);
    }
    return (
      <View className="container">
        <View className="icon-label">
          <Text className={`icon ${iconName}`}></Text>
          <View style={{ display: 'inline-block' }}>
            <View className="textContainer">
              {tArr.map((t, idx) => {
                return <Text key={idx} className="text">{t}</Text>
              })}
            </View>
          </View>
          <Text className="mao-hao">：</Text>
        </View>
        <Text className="value">{this.props.value}</Text>
      </View>
    );
  }
}

FlexRowLabelWithIcon.defaultProps = {
  text: ''
}