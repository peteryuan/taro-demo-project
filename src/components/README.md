# components

这个文件夹内用于定义一些公共组件，定义此类组件，尽可能遵守以下原则

- 单一职责
- 数据自维护
- 传递属性明确
- 命名明确
- 添加使用说明（demo）和注释