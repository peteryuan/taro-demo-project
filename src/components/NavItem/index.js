import Taro, { Component } from '@tarojs/taro'
import './index.scss'
import { Text, View } from '@tarojs/components'

export default class NavItem extends Component {

  handleClick = () => {
    this.props.onClick && this.props.onClick();
  }

  render() {
    const { text } = this.props;
    return <View className="nav-item" onClick={this.handleClick}>
      <Text className="nav-text">{text}</Text>
      <Text className="nav-icon iconfont icon-baijiantou"></Text>
    </View>
  }
}