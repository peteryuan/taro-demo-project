import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import './index.scss'
import { LOGO_URL } from '../../constants';


export default class CommonBanner extends Component {
  render() {
    return (
      <View className="home-banner">
        <img className="banner-img"
          src={LOGO_URL} />
      </View>
    );
  }
}