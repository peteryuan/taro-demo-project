import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import './index.scss'
import { LOGO_URL } from '../../constants';


export default class CommonCarPic extends Component {
  render() {
    return (
      <View className="car-pic"></View>
    );
  }
}