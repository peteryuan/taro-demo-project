import cf from '../common-func';

/**
 * @description 从Header中提取出session 
 * @author peter.yuan
 * @param {*} header
 * @returns
 */
function extractSession(header) {
  return header['X-Auth-Token'];
}
/**
 * @description 根据网络返回状态码处理数据
 * @author peter.yuan
 * @param {*} res
 * @param {*} resolve
 * @param {*} reject
 */
function handleNetStatusCode(res, resolve, reject) {
  const { statusCode, errMsg, data, header } = res;
  const session = extractSession(header);
  session && cf.setSessionToStorage(session);
  statusCode === 200 ? resolve(data) : reject(data);
}
/**
 * @description 生成 请求需要的header
 * @author peter.yuan
 * @returns
 */
function genHeader() {
  let session = cf.getSessionFromStorage();
  let header = {
    'Content-Type': 'application/json',
  };
  if (session) {
    header['X-Auth-Token'] = session;
  }
  return header;
}

function request(url, method, options = { data: {}, showLoading: true }) {
  return new Promise((resolve, reject) => {
    const { data, showLoading } = options;
    if (showLoading) {
      wx.showLoading({
        title: '加载中...', //提示的内容,
        mask: true, //显示透明蒙层，防止触摸穿透
      });
    }
    wx.request({
      url, //开发者服务器接口地址",
      data, //请求的参数",
      method,
      header: genHeader(),
      dataType: 'json', //如果设为json，会尝试对返回的数据做一次 JSON.parse
      success: res => {
        handleNetStatusCode(res, resolve, reject);
      },
      fail: (err) => {
        reject(err);
      },
      complete: () => {
        if (showLoading) wx.hideLoading();
      }
    });
  })
}

export function get(url, options) {
  return request(url, 'GET', options);
}
export function post(url, options) {
  return request(url, 'POST', options);
}
export function put(url, options) {
  return request(url, 'PUT', options);
}

export default {
  get,
  post,
  put
}