import Taro, { Component } from '@tarojs/taro'
import Index from './pages/index'
import '@tarojs/async-await'//IMPORTANT:想要在代码中使用promise async 请安装改以来并在app.js中引用
import './app.scss'
import homeStore from './store/home'
import { Provider } from '@tarojs/mobx'
import sa from 'sa-sdk-javascript';
import { initSa } from './lib/sa-js-sdk';
// import sa from './lib/sa/sensorsdata'
// window.__sa = sa;
// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5') {
  require('nerv-devtools')
}
const store = {
  homeStore
}

initSa();

class App extends Component {

  config = {
    pages: [
      'pages/index/index',
      'pages/home/index',
      'pages/activity/index',
      'pages/activity-info/index',
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    }
  }

  componentDidMount() {
  }

  componentDidShow() { }

  componentDidHide() { }

  componentDidCatchError() { }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
