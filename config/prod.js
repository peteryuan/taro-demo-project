module.exports = {
  env: {
    NODE_ENV: '"production"'
  },
  defineConstants: {
    APP_ID: '"BBBBB"'
  },
  weapp: {},
  h5: {
    router: {
      mode: 'hash',
      customRoutes: {
        '/pages/index/index': '/index',
        '/pages/home/index': '/home',
        '/pages/activity/index': '/activity',
      }
    }
  }
}
