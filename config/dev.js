module.exports = {
  env: {
    NODE_ENV: '"development"'
  },
  defineConstants: {
    APP_ID: '"wx4523437483b4d7a9"',
    HOST: '"https://l2l-test.boldseas.com"',
    TENANT_ID: '"boldseas"',
    SA_ENV: '"default"'
  },
  weapp: {
  },
  h5: {
    router: {
      mode: 'hash',
      customRoutes: {
        '/pages/index/index': '/index',
        '/pages/home/index': '/home',
        '/pages/activity/index': '/activity',
      }
    }
  }
}
